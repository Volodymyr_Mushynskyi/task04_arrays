package com.mushynskyi;

import com.mushynskyi.genericstasks.Droid;
import com.mushynskyi.genericstasks.Ship;
import com.mushynskyi.logicaltasks.arrays.ArraysTasks;
import com.mushynskyi.logicaltasks.game.GameProcess;
import com.mushynskyi.myqueue.dequeue.MyDeq;
import com.mushynskyi.myqueue.dequeue.QDroid;
import com.mushynskyi.myqueue.priorityqueue.MyPriorityQueue;
import com.mushynskyi.myqueue.priorityqueue.Wallet;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Main {
  private static Logger logger = LogManager.getLogger(Main.class);

  public static void main(String[] args) {
    MyDeq<QDroid> myDeq = new MyDeq<>();
    myDeq.addFirst(new QDroid("A"));
    myDeq.addFirst(new QDroid("B"));
    myDeq.addFirst(new QDroid("C"));
    myDeq.addLast(new QDroid("H"));
    System.out.println();
    for (QDroid d : myDeq) {
      logger.info(d);
    }
    logger.info("Remove first value");
    myDeq.deleteFromTop();
    for (QDroid d : myDeq) {
      logger.info(d);
    }

    MyPriorityQueue<Wallet> myPriorityQueue = new MyPriorityQueue<>();
    Wallet a = new Wallet("Mike", 50);
    myPriorityQueue.add(a);
    myPriorityQueue.add(new Wallet("Mo", 22));
    myPriorityQueue.add(new Wallet("Red", 100));
    myPriorityQueue.add(new Wallet("Sam", 15));
    myPriorityQueue.add(new Wallet("Sm", 5));
    myPriorityQueue.add(new Wallet("Sa", 150));
    myPriorityQueue.add(new Wallet("John", 60));
    myPriorityQueue.add(new Wallet("Jo", 200));
    myPriorityQueue.getAll();
    logger.info("Deleting one element");
    myPriorityQueue.remove(a);
    myPriorityQueue.getAll();

    new GameProcess().startGame();

    new ArraysTasks().createThirdArray();

    Ship<Droid> ship = new Ship<>();
    ship.put(new Droid("A"));
    ship.put(new Droid("C"));
    ship.put(new Droid("B"));
    ship.get(0);
  }
}
