package com.mushynskyi.genericstasks;

public class Droid extends Ship{

  private String Name;

  public Droid(String name) {
    Name = name;
  }

  public String getName() {
    return Name;
  }

  public void setName(String name) {
    Name = name;
  }
}
