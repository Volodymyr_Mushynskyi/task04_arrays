package com.mushynskyi.genericstasks;

import com.mushynskyi.logicaltasks.game.GameProcess;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

public class Ship<T extends Droid>  {
  private static Logger logger1 = LogManager.getLogger(GameProcess.class);

  List<T> droids = new ArrayList<>();

  public void put(T droid){
    droids.add(droid);
  }

  public void get(int in){
   logger1.info(droids.get(in).getName());
  }

  @Override
  public String toString() {
    return "Ship{" +
            "droids=" + droids +
            '}';
  }
}
