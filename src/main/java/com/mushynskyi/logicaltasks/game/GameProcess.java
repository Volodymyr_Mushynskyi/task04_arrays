package com.mushynskyi.logicaltasks.game;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Random;

public class GameProcess {
  private Integer[] door = new Integer[10];
  private int count = 0;

  private static Logger logger1 = LogManager.getLogger(GameProcess.class);

  private void createRandomDoor() {
    Random random = new Random();
    for (int i = 0; i < 10; i++) {
      door[i] = random.nextInt(80 + 100) - 100;
    }
  }

  private void printDoor() {
    logger1.info("-----------------------");
    for (int i = 0; i < 10; i++) {
      logger1.info("|Door " + i + "| Power = " + door[i] + "\t |");
    }
    logger1.info("-----------------------");
  }

  public void startGame() {
    Hero hero = new Hero(25);
    createRandomDoor();
    printDoor();
    countDeathDoor(door, 9);
    chooseDoor(new Hero(25), 3);
    chooseDoor(new Hero(25), 9);
  }

  private void countDeathDoor(Integer[] door, int size) {
    if (size == -1) {
      logger1.info("There are : " + count + " death doors");
      return;
    }
    if (door[size] > 0) {
      logger1.info("Number of Door that save live of the hero : " + size);
    }
    if (door[size] < 0 && (Math.abs(door[size]) - 25) > 0) {
      ++count;
    }
    countDeathDoor(door, size - 1);
  }

  private void chooseDoor(Hero hero, int choose) {
    if (hero.getPower() + door[choose] > 0) {
      hero.setPower(hero.getPower() + door[choose]);
      logger1.info("YOU WIN !!!");
      logger1.info("Power of the hero : " + hero.getPower());
    }
    if (hero.getPower() + door[choose] < 0 && door[choose] < 0) {
      hero.setPower(hero.getPower() + door[choose]);
      logger1.info("YOU LOSE !!!");
      logger1.info("Power of the hero : " + hero.getPower());
    }
    if (hero.getPower().equals(door[choose])) {
      logger1.info("YOU WIN !!!");
      logger1.info("Power of the hero : " + hero.getPower());
    }
  }
}
