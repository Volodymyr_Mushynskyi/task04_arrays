package com.mushynskyi.logicaltasks.game;

public class Hero {
  private Integer power;

  public Hero(Integer power) {
    this.power = power;
  }

  public Integer getPower() {
    return power;
  }

  public void setPower(Integer power) {
    this.power = power;
  }
}
