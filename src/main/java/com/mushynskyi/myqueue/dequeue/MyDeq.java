package com.mushynskyi.myqueue.dequeue;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Iterator;

public class MyDeq<T> implements Iterable<T> {

  private static Logger logger = LogManager.getLogger(MyDeq.class);

  private int sizeOfDeque;

  private Node<T> top;

  private Node<T> rear;

  public MyDeq() {
    sizeOfDeque = 0;
    top = new Node<T>();
    rear = new Node<T>();
    top.next = rear;
    rear.previously = top;
  }

  public void addFirst(T object) {
    if (object == null) {
      logger.error("You want to add object that doesn't exist");
    }
    Node<T> first = top.next;
    Node<T> node = new Node<T>();
    node.value = object;
    node.next = first;
    node.previously = top;
    top.next = node;
    first.previously = node;
    sizeOfDeque++;
  }

  public void addLast(T object) {
    if (object == null) {
      logger.error("You want to add object that doesn't exist");
    }
    Node<T> last = rear.previously;
    Node<T> node = new Node<T>();
    node.value = object;
    node.next = rear;
    node.previously = last;
    rear.previously = node;
    last.next = node;
    sizeOfDeque++;
  }

  public boolean isEmpty() {
    return sizeOfDeque == 0;
  }

  public Iterator<T> iterator() {
    return new DequeIterator();
  }

  public T deleteFromTop() {
    if (isEmpty()) {
      logger.info("Your deq is empty");
    }
    Node<T> first = top.next;
    Node<T> next = first.next;
    next.previously = top;
    top.next = next;
    sizeOfDeque--;
    return first.value;
  }

  public final T deleteFromEnd() {
    if (isEmpty()) {
      logger.info("Your deq is empty");
    }
    Node<T> last = rear.previously;
    Node<T> prev = last.previously;
    rear.previously = prev;
    prev.next = rear;
    sizeOfDeque--;

    return last.value;
  }

  private class DequeIterator implements Iterator<T> {

    private Node<T> current = top.next;

    public boolean hasNext() {
      return current != rear;
    }

    public T next() {
      if (!hasNext()) {
        throw new java.util.NoSuchElementException();
      }
      final T item = current.value;
      current = current.next;
      return item;
    }
  }

  private static class Node<T> {
    private T value;
    private Node<T> next;
    private Node<T> previously;
  }
}
