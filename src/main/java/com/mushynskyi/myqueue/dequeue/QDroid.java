package com.mushynskyi.myqueue.dequeue;

import java.util.Objects;

public class Droidq implements Comparable<Droidq>{

  private String name;

  public Droidq(){

  }

  public Droidq(String name) {
    this.name = name;
  }

  public String getName() {
    return this.name;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Droidq droidq = (Droidq) o;
    return Objects.equals(name, droidq.name);
  }

  @Override
  public int hashCode() {
    return Objects.hash(name);
  }

  @Override
  public String toString() {
    return "Droidq{" +
            "name='" + name + '\'' +
            '}';
  }

  @Override
  public int compareTo(Droidq droidq) {
    return this.getName().compareTo(droidq.getName());
  }
}
