package com.mushynskyi.myqueue.priorityqueue;

import com.mushynskyi.constants.Constants;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

public class MyPriorityQueue<T extends Comparable<T>> implements Queue<T> {

  private Comparator<? super T> comparator;

  private int sizeOfQueue;

  private T[] queueArray;

  private int top;

  private int rear;

  private int index;

  private static Logger logger = LogManager.getLogger(MyPriorityQueue.class);

  public MyPriorityQueue() {
    queueArray = (T[]) new Comparable[Constants.PQ_SIZE];
    sizeOfQueue = 0;
    top = 0;
    rear = 0;
    index = 0;
  }

  public void insert(T t) {
    int j;
    if (sizeOfQueue == 0) {
      queueArray[sizeOfQueue++] = t;
    } else {
      for (j = sizeOfQueue - 1; j >= 0; j--) {
        if (lessThan(queueArray[j], t)) {
          queueArray[j + 1] = queueArray[j];
        } else {
          break;
        }
      }
      queueArray[j + 1] = t;
      sizeOfQueue++;
    }
  }

  private boolean lessThan(Object a, T b) {
    int compareResult = compare((T) a, b);
    if (compareResult == 1) {
      return true;
    }
    return false;
  }

  private int compare(T firstKey, T secondKey) {
    if (comparator != null)
      return comparator.compare(firstKey, secondKey);
    try {
      Comparable<? super T> comparatorKey = (Comparable<? super T>) firstKey;
      return comparatorKey.compareTo(secondKey);
    } catch (ClassCastException e) {
      throw new ClassCastException(e.getMessage());
    }
  }

  public void getAll() {
    for (int i = 0; i < sizeOfQueue; i++) {
      logger.info(queueArray[i]);
    }
  }

  @Override
  public int size() {
    return sizeOfQueue;
  }

  @Override
  public boolean isEmpty() {
    return false;
  }

  @Override
  public boolean contains(Object o) {
    return false;
  }

  @Override
  public Iterator<T> iterator() {
    return null;
  }

  @Override
  public Object[] toArray() {
    return new Object[0];
  }

  @Override
  public <T1> T1[] toArray(T1[] a) {
    return null;
  }

  @Override
  public boolean remove(Object o) {
    for (int i = 0; i < sizeOfQueue; i++) {
      if (queueArray[i].equals(o)) {
        for (int j = i; j < sizeOfQueue - 1; j++) {
          queueArray[j] = queueArray[j + 1];
        }
      }
    }
    sizeOfQueue--;
    return true;
  }

  @Override
  public boolean containsAll(Collection<?> c) {
    return false;
  }

  @Override
  public boolean addAll(Collection<? extends T> c) {
    return false;
  }

  @Override
  public boolean removeAll(Collection<?> c) {
    return false;
  }

  @Override
  public boolean retainAll(Collection<?> c) {
    return false;
  }

  @Override
  public void clear() {

  }

  @Override
  public boolean add(T t) {
    if (t != null) {
      insert(t);
    }
    return false;
  }

  @Override
  public boolean offer(T t) {
    return false;
  }

  @Override
  public T remove() {
    return null;
  }

  @Override
  public T poll() {
    return null;
  }

  @Override
  public T element() {
    return null;
  }

  @Override
  public T peek() {
    return null;
  }
}