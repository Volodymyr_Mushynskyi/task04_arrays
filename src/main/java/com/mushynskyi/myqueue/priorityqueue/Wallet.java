package com.mushynskyi.myqueue.priorityqueue;

import java.util.Objects;

public class Wallet implements Comparable<Wallet> {
  String name;
  Integer count;

  public Wallet(String name, Integer count) {
    this.name = name;
    this.count = count;
  }

  public String getName() {
    return name;
  }

  public Integer getCount() {
    return count;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Wallet wallet = (Wallet) o;
    return Objects.equals(name, wallet.name) &&
            Objects.equals(count, wallet.count);
  }

  @Override
  public int hashCode() {
    return Objects.hash(name, count);
  }

  @Override
  public String toString() {
    return "Wallet{" +
            "name='" + name + '\'' +
            ", count=" + count +
            '}';
  }

  @Override
  public int compareTo(Wallet o) {
    return this.getCount().compareTo(o.getCount());
  }
}
